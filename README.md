**ScripTally**

|           |                                                                         |
|:----------|:------------------------------------------------------------------------|
| Version   | 1.0.0                                                                   |
| Changes   | hhttps://gitlab.com/viharm/scriptally/-/blob/master/CHANGELOG.md        |
| Download  | https://gitlab.com/viharm/ScripTally/tags                               |
| Issues    | https://gitlab.com/viharm/ScripTally/issues                             |
| License   | BSD (3-clause)                                                          |
| Language  | [POSIX Bourne shell](http://pubs.opengroup.org/onlinepubs/9699919799/)  |


# Introduction

*ScripTally* is a *POSIX*-compliant shell library to keep a tally of steps started and completed in any script

Once initialized for the total number of steps, it provides simple functions to keep a tally of the number of steps completed in any script.

It keeps a separate account of steps started and steps completed.

This behaviour can be very useful to understand exactly which steps have failed to complete successfully. Moreover, it can also help to debug which steps started execution to pinpoint any faults.

The tally of steps completed can simply be used as an exit status of the downstream scripts to identify any issues.


# Operational Logic

The core logic is to utilize a binary map of bits, with each bit representing stage of execution. The exit status starts as a map of binary 1's and the start status starts at zero (0). At the start of each sequential stage, the start status increments by one. Upon the successful completion of each sequential stage the exit status decrements by 1. Based on this approach, if all steps/stages are completed successfully, the exit status decrements to zero, which is the universal method for communicating successful completion.

If any stage fails to complete successfully, the specific bit for that stage in the global exit status would remain 1, thus immediately revealing the stage which failed.





# Installation

No installation required.


## Pre-requisites

* Standard POSIX utilities
    * `test`
    * `if`-`elif`-`else`-`fi`
    * `date`
    * `unset`
    * `echo`
    * `return`
    * `exit`


## Download


### Files archive

Get the release archives from the download link provided at the top of this page.


### Clone

Clone repository.

```
git clone https://gitlab.com/viharm/ScripTally.git
```


## Deploy

Save the downloaded directory structure in your choice of path within your application.


# Configuration

No specific configuration required yet.


# Usage

Include the library in your shell script...
```
. $(dirname $(readlink -f $0))/source/ScripTally.sh
```
(remember to change the sub-directory `source` to your chosen location for libraries)

The following workflow should be followed.


## Initialization

Initialize *ScripTally* with the number of stages/steps that need to be tracked (e.g., `6` steps),

```
fn_ScripTally_InitStatus 6
```

This function is not optional.

This function accepts only one numeric argument. There is no default value.


## Start of a Stage/Step

At the start of every stage/step of the script, simply call the function to increment the start status (e.g., for the first stage/step)
```
fn_ScripTally_StartStatusIncrement 1
Normal code...
```

This function is optional.

This function accepts at least one & up to two sequential numeric arguments in the following order


### Current Stage Number

This argument specificies explicitly which stage should be tracked.

This argument is optional.

If not specified, the current global stage number is incremented.

**Note**: If this argument is not used, then *ScripTally* will simply increment the global start status every time a stage/step is started. This way, *ScripTally* can only count the number of stages started. However if this argument is specified for every stage, *ScripTally* can codify the global start status to identify exactly which stage was started. Hence, it is highly recommended to use this argument.


### Input Start Status [Advanced Usage]

This argument specifies, explicitly, the input start status. This can be used to override the current global tally of start status.

This argument is optional.

If not specified, the current global start status is used.


## End of a Stage/Step

Similarly, at the end of every stage/step, the downstream script would call a simple function to decrement the exit status.
```
... normal code.
fn_ScripTally_ExitStatusDecrement 1
```

This function is optional.

This function accepts one & up to three sequential arguments in the following order.


### Current Stage Number

This argument specificies explicitly which stage should be tracked.

This argument is optional.

If not specified, the current global stage number is incremented.

**Note**: If this argument is not used, then *ScripTally* will simply decrement the global exit status every time a stage/step is completed. This way, *ScripTally* can only count the number of stages completed. However if this argument is specified for every stage, *ScripTally* can codify the global exit status to identify exactly which stage was completed successfully. Hence, it is highly recommended to use this argument.


### Input Maximum Stage Count [Advanced Usage]

This argument specifies, explicitly, the maximum number of stages. This can be used to override the maximum stage count specified during initialization.

This argument is optional.

If not specified, the global number of steps/stages (as initialized) is used.


### Input Exit Status [Advanced Usage]

This argument specifies, explicitly, the input exit status. This can be used to override the current global tally of exit status.

This argument is optional.

If not specified, the current global exit status is used.


## Checking Status

The current state of the start and exit statuses can be easily interrogated by checking the global variables...
```
${nm_ScripTally_Global_StartStatus}
```
```
${nm_ScripTally_Global_ExitStatus}
```

A stringified map of the start and exit statuses is available in the following variables
```
${sr_ScripTally_Global_StartStatusMap}
```
```
${sr_ScripTally_Global_ExitStatusMap}
```


## Utilizing the Status

It is left up to the downstream to utilize this information as per the style of user. Typically the final exit status can be used as the exit status of the downstream script.
```
... final line of code.
exit ${nm_ScripTally_Global_ExitStatus}
```


## Examples

### Basic Usage to Count Steps for a Six-Step Script

In this example the start status is initialized as `0` and the exit status as `63` (⇒ `111111`)

It is assume that...
  - step three was skipped in error; and
  - step five had an error, so the script exits during step five.

```
fn_ScripTally_InitStatus 6
echo "Other initialization code..."
echo "..."
echo "..."

# Define cleanup function
Cleanup() {
  echo "Normal code for the cleanup function..."
  echo "..."
  echo "..."
  exit ${nm_ScripTally_Global_ExitStatus}
}

# Declare trap
trap Cleanup 0 1 2 5 15

# First Stage/Step of Script
fn_ScripTally_StartStatusIncrement
echo "Normal code for the first stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement


# Second Stage/Step of Script
fn_ScripTally_StartStatusIncrement
echo "Normal code for the second stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement

# Third Stage/Step of Script - assume that this entire stage was skipped in error
fn_ScripTally_StartStatusIncrement
echo "Normal code for the third stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement

# Fourth Stage/Step of Script
fn_ScripTally_StartStatusIncrement
echo "Normal code for the fourth stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement

# Fifth Stage/Step of Script
fn_ScripTally_StartStatusIncrement
echo "Normal code for the fifth stage/step..."
echo "This stage has some error in it, so the execution will end before completion of this stage"
echo "..."
fn_ScripTally_ExitStatusDecrement

# Sixth Stage/Step of Script
fn_ScripTally_StartStatusIncrement
echo "Normal code for the sixth stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement

```

At the end of execution...
  - the start status would be `15` (⇒ `1111`), indicating that four stages were successfully started (1, 2, 4 & 5).
  - the exit status would be `56` (⇒ `111000`) indicating that three stages were successfully completed (1, 2 & 4).


### Basic Usage to Track a Six-Step Script

In this example the start status is initialized as `0` and the exit status as `63` (⇒ `111111`)

It is assume that...
  - step three was skipped in error; and
  - step five had an error, so the script exits during step five.

```
fn_ScripTally_InitStatus 6
echo "Other initialization code..."
echo "..."
echo "..."

# Define cleanup function
Cleanup() {
  echo "Normal code for the cleanup function..."
  echo "..."
  echo "..."
  exit ${nm_ScripTally_Global_ExitStatus}
}

# Declare trap
trap Cleanup 0 1 2 5 15

# First Stage/Step of Script
fn_ScripTally_StartStatusIncrement 1
echo "Normal code for the first stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement 1


# Second Stage/Step of Script
fn_ScripTally_StartStatusIncrement 2
echo "Normal code for the second stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement 2

# Third Stage/Step of Script - assume that this entire stage was skipped in error
fn_ScripTally_StartStatusIncrement 3
echo "Normal code for the third stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement 3

# Fourth Stage/Step of Script
fn_ScripTally_StartStatusIncrement 4
echo "Normal code for the fourth stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement 4

# Fifth Stage/Step of Script
fn_ScripTally_StartStatusIncrement 5
echo "Normal code for the fifth stage/step..."
echo "This stage has some error in it, so the execution will end before completion of this stage"
echo "..."
fn_ScripTally_ExitStatusDecrement 5

# Sixth Stage/Step of Script
fn_ScripTally_StartStatusIncrement 6
echo "Normal code for the sixth stage/step..."
echo "..."
echo "..."
fn_ScripTally_ExitStatusDecrement 6

exit ${nm_ScripTally_Global_ExitStatus}

```

At the end of execution...
  - the start status would be `27` (⇒ `11011`), indicating explicitly that stages 1,2,4 & 5 were successfully started (`0` for the third bit indicates that the third stage did not start).
  - the exit status would be `52` (⇒ `110100`) indicating explictly that stages 1,2 & 4 were successfully completed (`1`s for the third, fifth & sixth bits indicate that the third, fifth and sixt stages failed to complete successfully).



# Known limitations

* [Open issues](https://gitlab.com/viharm/ScripTally/-/issues?state=opened)
* [Issues which won't be fixed](https://gitlab.com/viharm/scriptally/-/issues?label_name%5B%5D=Outcome_Wontfix&scope=all&state=all)


# Support

For issues, queries, suggestions and comments please create an issue by using the link provided at the top of this page.


## Debugging

To keep the script size small & the code simple, *ScripTally* does not have built-in debugging options. You may use *[SciptScribe](https://gitlab.com/viharm/scriptscribe)* for debugging.


# Contribute

Please feel free to clone/fork and contribute via pull requests. Donations also welcome, simply create an issue by using the link provided at the top of this page.

Please make contact for more information.


# Environments

Developed on..

* *Debian Buster*

Known to be working on 

* *Debian Buster*


# Licence

Licensed under the modified BSD (3-clause) licence.

Copyright (c) 2022, Viharm
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice,
  this list of conditions and the following disclaimer in the documentation
  and/or other materials provided with the distribution.

* Neither the name of the copyright holder nor the names of its
  contributors may be used to endorse or promote products derived from
  this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


# Credits

### Utilities

#### VS Code

*Visual Studio Code* code editor, used under the *Microsoft Software License*.


#### VS Code Server

Web based *Visual Studio Code* code editor, used under the MIT license.

Copyright (C) Coder Technologies Inc.


#### Ungit

*Ungit* client for *Git* (https://github.com/FredrikNoren/ungit) used under the MIT license

Copyright (C) Fredrik Norén


#### Notepad++

*Notepad++* text editor (https://notepad-plus-plus.org/), used under the GNU GPL v2.

Copyright (C) Don Ho.


#### GitLab

Hosted by *GitLab* code repository (gitlab.com).


### Guidance

#### Changelog

Guidance on standardizing changelog provided by the [Keep a Changelog](https://keepachangelog.com), used under the MIT License.


#### Licensing

Guidance on choice of license provided by the [Open Source Initiative](https://opensource.org/licenses).