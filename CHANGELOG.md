# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Unreleased]

### Security

### Fixed

### Changed

### Added

### Deprecated

### Removed


## [1.0.0] - 2017-06-20

### Added
- Base functionality


[Unreleased]: https://gitlab.com/viharm/ScripTally/-/compare/v1.0.0...dev
[1.0.0]: https://gitlab.com/viharm/ScripTally/-/tags