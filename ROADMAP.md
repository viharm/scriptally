# Roadmap
All future changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [Future]

### Security

### Fixed

### Changed

### Added

- Decrement Exit Status: Add check for existing current stage number; Used when not provided.
- Decrement Exit Status: Add check for existing maximum stage count; Used when not provided.
- Decrement Exit Status: Add check for existing current global exit status; Used when not provided.
- Increment Start Status: Add check for existing current stage number; Used when not provided.
- Increment Start Status: Add check for existing current global start status; Used when not provided.


### Deprecated

### Removed

