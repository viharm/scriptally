#!/bin/sh

  ## \package    ScripTally
  #  \author     viharm
  #  \version    1.0.0
  #  \date       2022-10-01
  #  \brief      ScripTally is a POSIX-compliant shell library to keep a tally of steps started and completed in any script
  #  \detail     Once initialized for the total number of steps, it provides simple functions to keep a tally of the number of steps completed in any script.
  #              It keeps a separate account of steps started and steps completed.
  #  \copyright  Copyright (c) 2022, viharm
  #  \license    Modified BSD (3-clause) licence.


  # ===== Define Variables =====

  # ==== Binaries & Scripts Paths ====

  sr_ScripTally_BCBin=$(command -v bc)


  # ===== Declare Functions =====

  # ==== Decimal to Binary Converter ====

  fn_ScripTally_Dec2Bin() {
    nm_ScripTally_Dec2Bin_Input=${1}
    echo "ibase=10;obase=2;${nm_ScripTally_Dec2Bin_Input}" | ${sr_ScripTally_BCBin}
  }


  # ==== Exponentiate ====
  
  # Function to raise value to the power of an exponent.
  fn_ScripTally_Exponentiate() {

    #Initialize loop counter.
    nm_ScripTally_Exponentiate_Counter_01=$((1))
    # Save first argument as the base.
    nm_ScripTally_Exponentiate_Base=${1}
    # Save second argument as the exponent.
    nm_ScripTally_Exponentiate_Exponent=${2}

    # Check if both base and exponent are zero
    if [ ${nm_ScripTally_Exponentiate_Base} -eq 0 -a ${nm_ScripTally_Exponentiate_Exponent} -eq 0 ]; then
      # If both base & exponent are zero, then the result is indeterminate.
      nm_ScripTally_Exponentiate_Result="0^0"
    else
      # If both base & exponent are not zero, then check if exponent is zero.
      if [ ${nm_ScripTally_Exponentiate_Exponent} -eq 0 ]; then
        # If exponent is zero, then the result is one.
        nm_ScripTally_Exponentiate_Result=$((1))
      else
        # If exponent is non-zero, then initialize result as the base itself.
        nm_ScripTally_Exponentiate_Result=${nm_ScripTally_Exponentiate_Base}
        # Multiply the base by itself, exponent number of times.
        while test ${nm_ScripTally_Exponentiate_Counter_01} -lt ${nm_ScripTally_Exponentiate_Exponent}; do
          nm_ScripTally_Exponentiate_Counter_01=$((${nm_ScripTally_Exponentiate_Counter_01}+1))
          nm_ScripTally_Exponentiate_Result=$((${nm_ScripTally_Exponentiate_Result}*${nm_ScripTally_Exponentiate_Base}))
        done
      fi
    fi

    # Remove ephemeral function variables.
    unset nm_ScripTally_Exponentiate_Counter_01
    unset nm_ScripTally_Exponentiate_Base
    unset nm_ScripTally_Exponentiate_Exponent

    # Return the result as an echo.
    echo ${nm_ScripTally_Exponentiate_Result}
  }


  # ==== Initialization ====

  # Function to establish maximum exit status & minimum start status values to start with.
  fn_ScripTally_InitStatus() {

    # Capture maximum number of stages from first argument.
    nm_ScripTally_Global_MaxStageCount=${1}

    # Establish starting stage number.
    nm_ScripTally_Global_CurrStageNum=$((0))

    # Define start status as zero.
    nm_ScripTally_Global_StartStatus=$((0))
    sr_ScripTally_Global_StartStatusMap=$(fn_ScripTally_Dec2Bin ${nm_ScripTally_Global_StartStatus})

    # Calculate the initial exit status based on the number of stages.
    nm_ScripTally_InitStatus_MaxCount=$(fn_ScripTally_Exponentiate 2 ${nm_ScripTally_Global_MaxStageCount})
    nm_ScripTally_Global_MaxExitStatus=$((${nm_ScripTally_InitStatus_MaxCount}-1))

    # Set starting value of the global exit status to the max exit status.
    nm_ScripTally_Global_ExitStatus=${nm_ScripTally_Global_MaxExitStatus}
    sr_ScripTally_Global_ExitStatusMap=$(fn_ScripTally_Dec2Bin ${nm_ScripTally_Global_ExitStatus})

    unset nm_ScripTally_InitStatus_MaxCount

  }


  # ==== Decrement Exit Status ====

  # Function for decrementing the exit status after each successful stage.
  fn_ScripTally_ExitStatusDecrement() {

    # Save the number of arguments provided to the function.
    nm_ScripTally_ExitStatusDecrement_ArgCount=${#}

    # Establish the minimum requirements to proceed with the function.
    # 1. Current stage number
    # 2. Max exit status. This is calculated from max stage count (it's just easier for a user to input the max num of stages rather than having to calculate the decimal max exit status).
    # 3. Current exit status.

    # For the current stage number, check if at least one argument is provided.
    if [ ${nm_ScripTally_ExitStatusDecrement_ArgCount} -ge 1 ]; then
      # If at least one argument is provided then save the first argument as the stage number to use.
      nm_ScripTally_ExitStatusDecrement_InputStageNum=${1}
    else
      # If no arguments are provided then take the stage number from the current global stage number.

      # Check if current stage number exists
      # FIXME
      # FIXME

      nm_ScripTally_ExitStatusDecrement_InputStageNum=${nm_ScripTally_Global_CurrStageNum}
    fi

    # For the max stage count, check if at least two arguments are provided.
    if [ ${nm_ScripTally_ExitStatusDecrement_ArgCount} -ge 2 ]; then
      # If at least two arguments are provided then save the second argument as the maximum stage count to use.
      nm_ScripTally_ExitStatusDecrement_InputMaxStageCount=${2}
    else
      # If a second argument is not provided then take the maximum stage count from the global maximum stage count.

      # Check if global max stage count exists
      # FIXME
      # FIXME

      nm_ScripTally_ExitStatusDecrement_InputMaxStageCount=${nm_ScripTally_Global_MaxStageCount}
    fi

    # For the current exit status, check if at least three arguments are provided.
    if [ ${nm_ScripTally_ExitStatusDecrement_ArgCount} -ge 3 ]; then
      # If at least three arguments are provided then save the third argument as the input exit status.
      nm_ScripTally_ExitStatusDecrement_InputExitStatus=${3}
    else
      # If the third argument is not provided then take the exit status from the global exit status.

      # Check if global exit status exists
      # FIXME
      # FIXME

      nm_ScripTally_ExitStatusDecrement_InputExitStatus=${nm_ScripTally_Global_ExitStatus}
    fi

    # Check if global max exit status exists. Reverse logic to avoid the "else".
    if [ ! ${nm_ScripTally_Global_MaxExitStatus} -ge 0 ]; then
      # If the global max exit status does not exist, then establish maximum exit status based on the total number of stages.
      nm_ScripTally_ExitStatusDecrement_MaxCount=$(fn_ScripTally_Exponentiate 2 ${nm_ScripTally_ExitStatusDecrement_InputMaxStageCount})
      nm_ScripTally_Global_MaxExitStatus=$((${nm_ScripTally_ExitStatusDecrement_MaxCount}-1))
    fi

    # Create a mask.
    nm_ScripTally_ExitStatusDecrement_Exponent_01=$((${nm_ScripTally_ExitStatusDecrement_InputStageNum}-1))
    nm_ScripTally_ExitStatusDecrement_Offset_01=$(fn_ScripTally_Exponentiate 2 ${nm_ScripTally_ExitStatusDecrement_Exponent_01})
    nm_ScripTally_ExitStatusDecrement_ExitMask=$((${nm_ScripTally_Global_MaxExitStatus} - ${nm_ScripTally_ExitStatusDecrement_Offset_01}))

    # Apply mask for exit status.
    nm_ScripTally_ExitStatusDecrement_OutputExitStatus=$((${nm_ScripTally_ExitStatusDecrement_InputExitStatus} & ${nm_ScripTally_ExitStatusDecrement_ExitMask}))

    # Save global exit status.
    nm_ScripTally_Global_ExitStatus=${nm_ScripTally_ExitStatusDecrement_OutputExitStatus}
    sr_ScripTally_Global_ExitStatusMap=$(fn_ScripTally_Dec2Bin ${nm_ScripTally_Global_ExitStatus})

    # Unset ephemeral function variables.
    unset nm_ScripTally_ExitStatusDecrement_ArgCount
    unset nm_ScripTally_ExitStatusDecrement_InputStageNum
    unset nm_ScripTally_ExitStatusDecrement_InputMaxStageCount
    unset nm_ScripTally_ExitStatusDecrement_InputExitStatus
    unset nm_ScripTally_ExitStatusDecrement_MaxCount
    unset nm_ScripTally_ExitStatusDecrement_Exponent_01
    unset nm_ScripTally_ExitStatusDecrement_Offset_01
    unset nm_ScripTally_ExitStatusDecrement_ExitMask
    unset nm_ScripTally_ExitStatusDecrement_OutputExitStatus

  }


  # ==== Increment Start Status ====

  # Function for incrementing start status at the beginning of each stage.
  fn_ScripTally_StartStatusIncrement() {

    # Save the number of arguments provided to the function.
    nm_ScripTally_StartStatusIncrement_ArgCount=${#}

    # Establish the minimum requirements to proceed with the function.
    # 1. Current stage number.
    # 3. Current start status.

    # This function (to increment start status) is the best place to auto-increment the current stage number.
    # Increment the current stage number before it is used for further calcs.
    nm_ScripTally_Global_CurrStageNum=$((${nm_ScripTally_Global_CurrStageNum}+1))

    # Check if at least one argument is provided.
    if [ ${nm_ScripTally_StartStatusIncrement_ArgCount} -ge 1 ]; then
      # If at least one argument is provided then save the first argument as the stage number to use.
      nm_ScripTally_StartStatusIncrement_InputStageNum=${1}
    else
      # If no arguments are provided then take the stage number from the current global stage number.

      # Check if current stage number exists
      # FIXME
      # FIXME

      nm_ScripTally_StartStatusIncrement_InputStageNum=${nm_ScripTally_Global_CurrStageNum}
    fi

    # Check if at least two arguments are provided.
    if [ ${nm_ScripTally_StartStatusIncrement_ArgCount} -ge 2 ]; then
      # If at least two arguments are provided then save the second argument as the input start status.
      nm_ScripTally_StartStatusIncrement_InputStartStatus=${2}
    else
      # If a second argument is not provided then take the input start status from the global start status.

      # Check if global exit status exists
      # FIXME
      # FIXME

      nm_ScripTally_StartStatusIncrement_InputStartStatus=${nm_ScripTally_Global_StartStatus}
    fi

    # Creat a mask.
    nm_ScripTally_StartStatusIncrement_Exponent_01=$((${nm_ScripTally_StartStatusIncrement_InputStageNum}-1))
    nm_ScripTally_StartStatusIncrement_StartMask=$(fn_ScripTally_Exponentiate 2 ${nm_ScripTally_StartStatusIncrement_Exponent_01})

    # Apply mask for start status.
    nm_ScripTally_StartStatusIncrement_OutputStartStatus=$((${nm_ScripTally_StartStatusIncrement_InputStartStatus} + ${nm_ScripTally_StartStatusIncrement_StartMask}))

    # Save output as the global start status.
    nm_ScripTally_Global_StartStatus=${nm_ScripTally_StartStatusIncrement_OutputStartStatus}
    sr_ScripTally_Global_StartStatusMap=$(fn_ScripTally_Dec2Bin ${nm_ScripTally_Global_StartStatus})

    # Remove ephemeral function variables.
    unset nm_ScripTally_StartStatusIncrement_ArgCount
    unset nm_ScripTally_StartStatusIncrement_InputStageNum
    unset nm_ScripTally_StartStatusIncrement_InputStartStatus
    unset nm_ScripTally_StartStatusIncrement_Exponent_01
    unset nm_ScripTally_StartStatusIncrement_StartMask
    unset nm_ScripTally_StartStatusIncrement_OutputStartStatus

  }
